const dictionary = require("./dictionary")

const startGame = () => {
  return {
    status: "RUNNING",
    word: "TESTE",
    lives: 5,
    display_word: "_ _ _ _ _",
    guesses: [],
    message: "Por favor, tente uma letra"
  };
};

const takeGuess = (game_state, guess) => {

  if(!isValidInput(guess))
  return {
    ...game_state,
    status: "ERROR"
  }


  return {
    ...game_state,
    lives: game_state.lives + 1
  };
};

const isValidInput = (guess) => {
  return guess.length == 1
}

module.exports = {
  startGame,
  takeGuess
};
