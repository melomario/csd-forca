const GameEngine = require("./game_engine");
const readline = require("readline");

const linereader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const run = () => {
  const game = GameEngine.startGame()

  console.log("**********************");
  console.log("*  N E W    G A M E  *");
  console.log("**********************");
  game_loop(game);
};

const game_loop = game => {
  console.log(game.message)
  if(game.status === "RUNNING"){
    printGameState(game);
    evaluateGuess(game);
  }
  else{
    console.log("A palavra era: " + game.word)
    console.log(game.status)
  }
};

const printGameState = game => {
  console.log("Remaining Lives: " + game.lives);
  console.log( game.display_word);
  console.log( game.guesses );
};

const evaluateGuess = game => {
  linereader.question("Guess a letter: ", letter => {
    updated_game = GameEngine.takeGuess(game, letter);
    game_loop(updated_game);
  });
};

module.exports = {
  run
};
