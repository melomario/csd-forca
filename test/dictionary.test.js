const Dictionary = require("../lib/dictionary");

describe("O nosso dicionário deve", () => {
  test("retornar sempre uma palavra válida para o jogo", () => {
    expect(Dictionary.getRandomWord()).toBe("adepto");
  });
});
