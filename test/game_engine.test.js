const GameEngine = require("../lib/game_engine");

describe("O nosso jogo precisa", () => {
  test("começar com um estado válido", () => {
    let game_state = GameEngine.startGame()

    expect(game_state.lives).toBe(5)
    expect(game_state.status).toBe("RUNNING")

  });

  test("validar que o palpite do usuário tenha apenas uma letra", () => {
    let game_state = GameEngine.startGame()

    new_state = GameEngine.takeGuess(game_state, "abc123")

    expect(new_state.status).toBe("ERROR")
    expect(new_state.lives).toBe(game_state.lives)
  });

  // Grupo 1
  test("validar que o palpite do usuário não seja um número ou caracter especial", () => {

    let game_state = GameEngine.startGame()

    new_state = GameEngine.takeGuess(game_state, "!")

    expect(new_state.status).toBe("ERROR")
    expect(new_state.lives).toBe(game_state.lives)
  });

  // Grupo 2
  test("atualizar a palavra exibida caso o jogador acerte a letra", () => {

    // Aqui vocês podem utilizar um estado inicial fake, porque vão precisar saber a palavra
    game_state = {
      status: "RUNNING",
      word: "abc",
      lives: 1,
      display_word: "_ _ c",
      guesses: ['x', 'y', 'z'],
      message: "Por favor, tente uma letra"
    };

    let new_state = GameEngine.takeGuess(game_state,"a");
    expect(game_state.lives).toBe(new_state.lives);
    expect(game_state.guesses).toMatch(new_state.guesses);
    expect(game_state.status).toMatch(new_state.status);
    expect(new_state.display_word).toMatch("a _ c");
  });

  // Grupo 1
  test("atualizar o número de vidas caso o jogador erre a letra", () => {
    let game_state = GameEngine.startGame()

    let initialLives = game_state.lives

    new_state = GameEngine.takeGuess(game_state, "r")

    expect(new_state.lives).toBe(initialLives -1)


  });
  
  // Grupo 1
  test("finalizar o jogo com vitória caso o jogaodor acerte todas as letras", () => {

    game_state = {
      status: "RUNNING",
      word: "TESTE",
      lives: 1,
      display_word: "T E _ T E",
      guesses: ['x', 'y', 'z'],
      message: "Por favor, tente uma letra"
    };

    new_state = GameEngine.takeGuess(game_state, "S")

    expect(new_state.display_word).toBe("T E S T E")

    let status = "Victory" 

    expect(game_state.status).toBe(status)

  });

  // Grupo 2
  test("finalizar o jogo com derrota caso o jogaodor perca todas as vidas", () => {
    
    game_state = {
      status: "RUNNING",
      word: "abc",
      lives: 1,
      display_word: "_ b c",
      guesses: ['x', 'y', 'z'],
      message: "Por favor, tente uma letra"
    };
    
    new_state = GameEngine.takeGuess(game_state, "r");

    expect(new_state.status).toBe("GAMEOVER");

    expect(new_state.lives).toBe(0);
  });

});



